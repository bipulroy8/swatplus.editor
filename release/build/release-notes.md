* Major version release 2.0.0.
* We recommend users please visit the SWAT+ website to update your QSWAT+ version
* Updated to SWAT+ revision 60.5.2
* You can now save and load scenarios
* Added SWAT+ Check feature
* User interface reorganization and enhancements
* For projects made with QSWAT+ 1.3.1 and up, read aquifers and new routing from GIS tables during set up
* Set values for hydrology.hyd variables perco, cn3_swf, and latq_co based on soil hyd. group and HRU slope

View the full list of changes at swat.tamu.edu/plus