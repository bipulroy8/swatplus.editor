from flask_restful import Resource, reqparse, abort
from playhouse.shortcuts import model_to_dict
from peewee import *

from .base import BaseRestModel
from database.project import base as project_base
from database.project.setup import SetupProjectDatabase
from database.project.config import Project_config
from database.project.decision_table import D_table_dtl, D_table_dtl_act, D_table_dtl_act_out, D_table_dtl_cond, D_table_dtl_cond_alt
from database import lib
from helpers import table_mapper

class DTableDtlListApi(BaseRestModel):
	def get(self, project_db, table_type):
		table = D_table_dtl
		filter_cols = [table.name]
		SetupProjectDatabase.init(project_db)
		args = self.get_table_args()

		selected_table = table.select().where(table.file_name == table_type)

		total = selected_table.count()
		sort = self.get_arg(args, 'sort', 'name')
		reverse = self.get_arg(args, 'reverse', 'n')
		page = self.get_arg(args, 'page', 1)
		per_page = self.get_arg(args, 'per_page', 50)
		filter_val = self.get_arg(args, 'filter', None)

		if filter_val is not None:
			w = None
			for f in filter_cols:
				w = w | (f.contains(filter_val))
			s = selected_table.where(w)
		else:
			s = selected_table

		matches = s.count()

		sort_val = SQL('[{}]'.format(sort))
		if reverse == 'y':
			sort_val = SQL('[{}]'.format(sort)).desc()

		m = s.order_by(sort_val).paginate(int(page), int(per_page))
		ml = [{'id': v.id, 'name': v.name, 'conditions': len(v.conditions), 'actions': len(v.actions)} for v in m]

		return {
			'total': total,
			'matches': matches,
			'items': ml
		}


class DTableDtlApi(BaseRestModel):
	def get(self, project_db, id):
		return self.base_get(project_db, id, D_table_dtl, 'Decision table', back_refs=True, max_depth=2)
